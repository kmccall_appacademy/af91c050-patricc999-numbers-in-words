class Fixnum

  def in_words

    if self < 100
      under_hundred(self)
    elsif self < 1000
      under_thousand(self)
    elsif self < 1000000
      under_million(self)
    elsif self < 1000000000
      under_billion(self)
    elsif self < 1000000000000
      under_trillion(self)
    else
      over_trillion(self)
    end

  end

  def under_hundred(num)

    words = {
      0 => 'zero',
      1 => 'one',
      2 => 'two',
      3 => 'three',
      4 => 'four',
      5 => 'five',
      6 => 'six',
      7 => 'seven',
      8 => 'eight',
      9 => 'nine',
      10 => 'ten',
      11 => 'eleven',
      12 => 'twelve',
      13 => 'thirteen',
      14 => 'fourteen',
      15 => 'fifteen',
      16 => 'sixteen',
      17 => 'seventeen',
      18 => 'eighteen',
      19 => 'nineteen',
      20 => 'twenty',
      30 => 'thirty',
      40 => 'forty',
      50 => 'fifty',
      60 => 'sixty',
      70 => 'seventy',
      80 => 'eighty',
      90 => 'ninety'
    }

    ones_place = num % 10
    if num < 20 || ones_place == 0
      words[num]
    else
      words[num - ones_place] + ' ' + under_hundred(ones_place)
    end
  end

  def under_thousand(num)

    tens_place = num % 100
    hundreds_place = num.to_s.chars.first.to_i

    if tens_place == 0
      hundreds_place.in_words + ' hundred'
    else
      hundreds_place.in_words + ' hundred ' + tens_place.in_words
    end
  end

  def under_million(num)

    hundreds_place = num % 1000
    thousands_place = num.to_s.chars[0...-3].join.to_i

    if hundreds_place == 0
      thousands_place.in_words + ' thousand'
    else
      thousands_place.in_words + ' thousand ' + hundreds_place.in_words
    end
  end

  def under_billion(num)

    thousands_place = num % 1000000
    millions_place = num.to_s.chars[0...-6].join.to_i

    if thousands_place == 0
      millions_place.in_words + ' million'
    else
      millions_place.in_words + ' million ' + thousands_place.in_words
    end
  end

  def under_trillion(num)

    millions_place = num % 1000000000
    billions_place = num.to_s.chars[0...-9].join.to_i

    if millions_place == 0
      billions_place.in_words + ' billion'
    else
      billions_place.in_words + ' billion ' + millions_place.in_words
    end
  end

  def over_trillion(num)

    billions_place = num % 1000000000000
    trillions_place = num.to_s.chars[0...-12].join.to_i

    if billions_place == 0
      trillions_place.in_words + ' trillion'
    else
      trillions_place.in_words + ' trillion ' + billions_place.in_words
    end
  end

end
